<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB as FacadesDB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web', ['except' => [
            'create',
            'store',
            'verify_email'
            ]
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'password' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) :
            $errors = $validator->errors();
            if ($errors->has('name')) :
                toastr()->warning($errors->first('name'));
                FacadesDB::rollback();
                return back()->withInput($request->all());
            endif;
            if ($errors->has('last_name')) :
                toastr()->warning($errors->first('last_name'));
                FacadesDB::rollback();
                return back()->withInput($request->all());
            endif;
            if ($errors->has('email')) :
                toastr()->warning($errors->first('email'));
                FacadesDB::rollback();
                return back()->withInput($request->all());
            endif;
            if ($errors->has('password')) :
                toastr()->warning($errors->first('password'));
                FacadesDB::rollback();
                return back()->withInput($request->all());
            endif;
        endif;

        try {
            FacadesDB::beginTransaction();
            $user = new User();
            $user->name = $request->name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->password = hash::make($request->password);
            $user->save();
            FacadesDB::commit();
            return response()->json(1);
        } catch (QueryException $e) {
            toastr()->warning($e->getMessage());
            FacadesDB::rollback();
            return back()->withInput($request->all());
        } catch (ModelNotFoundException $e) {
            toastr()->warning($e->getMessage());
            FacadesDB::rollback();
            return back()->withInput($request->all());
        } catch (\Exception $e) {
            toastr()->warning($e->getMessage());
            FacadesDB::rollback();
            return back()->withInput($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $idUser = $id;
        return view('user.edit',compact('idUser'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modelUser = new User();
        $user = $modelUser->getUser($id);
        return response()->json( $user );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verify_email($email){
        $modelUser = new User();
        $verify_email = $modelUser->verify_email($email);
        return $verify_email == true ? 1 : 0; 
    }

    public function change_password(Request $request){
        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'idUser' => 'required'
        ]);
    
        if ($validator->fails()) {
            toastr()->warning($validator->errors()->first('idTask'));
            return back()->withInput($request->all());
        }
    
        try {
            FacadesDB::beginTransaction();
                User::where('idUser', $request->idUser)
                    ->update([
                        'password' => hash::make($request->password),
                        'updated_at' => Carbon::now(),
                    ]);
            FacadesDB::commit();
            return response()->json(1);
        } catch (\Exception $e) {
            toastr()->warning($e->getMessage());
            FacadesDB::rollback();
            return back()->withInput($request->all());
        }
    }
}
