<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'tasks';
    protected $primaryKey = 'idTask';
    protected $fillable = [
        'idUser',
        'task',
        'description',
        'finished'
    ];

    public function getTask($task = null)
    {
        $tasks = Task::where('deleted_at', null)
            ->select('tasks.*')
            ->where(function ($query) use ($task) {
                if ($task) :
                    $query->where('tasks.idTask', $task);
                endif;
            })
            ->get();
        return $tasks;
    }
}
