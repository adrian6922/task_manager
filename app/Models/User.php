<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $table = 'users';
    protected $primaryKey = 'idUser';
    protected $fillable = [
        'name',
        'last_name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getUser($user = null)
    {
        $users = User::where('deleted_at', null)
            ->select('users.*')
            ->where(function ($query) use ($user) {
                if ($user) :
                    $query->where('users.idUser', $user);
                endif;
            })
            ->get();

            if($user != null):
                foreach($users as $userData):
                    $userData->initialName = strtoupper($userData->name[0]);
                    $userData->initialLastName = strtoupper($userData->last_name[0]);
                endforeach;
            endif;
        return $users;
    }

    public static function verify_email($email)
    {
        return  User::where('email', $email)->count() > 0 ? true : false;
    }
}
