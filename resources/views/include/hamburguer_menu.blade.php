<nav class="navbar navbar-expand-md bg-success navbar-dark rounded">
    <div class="container-fluid">
        <button 
            class="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div 
            class="collapse navbar-collapse"
            id="navbarCollapse">
            <ul class="navbar-nav">
                <li class="nav-item item_menu">
                    <a 
                        class="nav-link"
                        href="{{ url('/tasks') }}">
                        <i class="fa-solid fa-clipboard-list"></i> Board
                    </a>
                </li>
                <li class="nav-item item_menu">
                    <a 
                        class="nav-link"
                        href="{{ url('users/'.Auth::user()->idUser) }}">
                        <i class="fas fa-user-circle"></i> Mi perfil
                    </a>
                </li>
                <li class="nav-item item_menu">
                    <a 
                        class="nav-link"
                        href="{{ route('logout') }}" 
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <i class="fa-solid fa-arrow-right-to-bracket"></i> Cerrar sesión
                    </a>
                    <form 
                        class="d-none"
                        id="logout-form"
                        action="{{ route('logout') }}"
                        method="POST">
                        @csrf
                    </form>
                </li>
            </ul>
        </div>
    </div>
</nav>