@extends('layouts.task.login')
@section('title','error')    
@section('content')
    <div class="container-fluid bg-success background_error">
        <div class="col-md-6 mx-auto text-center">
            <img 
                class="mt-5 rounded logo_img"
                src="{{ asset('assets/img/logo_prueba.png') }}"
                alt="logo_login"
            >
            <h1 class="text-light mt-4">Meteor Task</h1>        
        </div>
        <h2 class="text-center text-light">Error 404</h2>
        <h2 class="text-center text-light">¡Pagina no encontrada!</h1>
        <div 
            class="d-grid gap-2 col-10 col-sm-8 col-md-6 col-lg-4 mx-auto mt-4">
            <button 
                class="btn btn-outline-warning"
                type="button"
                id="back">
                Volver a la aplicación
            </button>              
        </div>
    </div>
@endsection
@section('script')
    <script>
        let button_back = document.getElementById('back');
        button_back.addEventListener('click', function(){
            window.history.back();
        })
    </script>
@endsection
