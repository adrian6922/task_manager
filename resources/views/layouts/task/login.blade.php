<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" type="image/x-icon" href="{{ asset('assets/img/logo_prueba.ico') }}"/>
        <title>Meteor Task | @yield('title')</title>
        @include('include.css')
    </head>
    <body>
        <main>
            @yield('content')
        </main>
        @yield('script')
        @include('include.script')
    </body>
</html>