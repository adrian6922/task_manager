@extends('layouts.task.login')
@section('title','login')    
@section('content')
    <div
        class="container-fluid"
        id="appLogin">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="rounded login">
                    <div class="text-center">
                        <img 
                            class="mt-1 rounded logo_img"
                            src="{{ asset('assets/img/logo_prueba.png') }}"
                            alt="logo_login"
                        >
                        <h3 class="text-center text-light mt-1">Meteor Task</h3>
                    </div>
                    <div class="col-md-9 mx-auto">
                        <form id="form_login">
                            @csrf
                            <div class="form-group mt-5">
                                <input 
                                    class="form-control"
                                    id="inputEmail"
                                    type="email"
                                    name="email"
                                    v-model="email"
                                    placeholder="Username"
                                >
                            </div>
                            <div class="form-group mt-5">
                                <input 
                                    class="form-control"
                                    id="inputPassword"
                                    type="password"
                                    name="password"
                                    v-model="password"
                                    placeholder="Password"
                                >
                            </div>
                            <div class="d-grid gap-2 col-10 col-sm-8 col-md-6 col-lg-4 mx-auto mt-4">
                                <button 
                                    class="btn btn-outline-warning"
                                    type="button"
                                    id="enter"
                                    @click.prevent="login">
                                    Login
                                </button>
                                <a 
                                    class="text-center text-light"
                                    href="{{ url('users/create') }}">
                                    Crear cuenta
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="d-none d-md-block col-12 col-md-6 img_login" id="image_login"></div>
        </div>
    </div>
@endsection
@section('script')    
@include('include.vue')
    <script>
        new Vue({
            el:"#appLogin",
            data:{
                email:'',
                password:''
            },
            methods: {
                login(){
                    let inputEmail = document.getElementById('inputEmail');
                    let inputPassword = document.getElementById('inputPassword');
                    let token = $('input[name=_token]').val();

                    if(this.validate_input(this.email)){
                        inputEmail.style.borderColor = 'red';
                        inputEmail.focus();
                        Swal.fire('¡Falta Usuario!','Ingrese usuario','warning');
                        return;
                    }else{
                        inputEmail.style.borderColor = '';
                    }

                    if(this.validate_input(this.password)){
                        inputPassword.style.borderColor = 'red';
                        inputPassword.focus();
                        Swal.fire('¡Falta Password!','Ingrese password','warning');
                        return;
                    }else{
                        inputPassword.style.borderColor = '';
                    }

                    axios.post('/login-user',{
                        _token  : token,
                        email   :this.email,
                        password:this.password
                    }).then(response=>{
                        if(response.data != 0 ){
                            this.cleanForm();
                            window.location.href = "{{ url('home') }}";
                        }else{
                            Swal.fire({
                                icon: 'error',
                                title: 'Usuario o contraseña incorrecta',
                            })
                            this.cleanForm();
                        }
                    }).catch(error=>{
                        console.log(error);
                    });
                },
                validate_input(input){
                    return input == '' ? true : false
                },
                cleanForm(){
                    return document.getElementById('form_login').reset();
                },
                pressEnter(){
                    document.addEventListener('keypress',(e)=>{
                        if(e.which == 13){
                            this.login();
                        }
                    })
                }
            },
            mounted() {
                this.pressEnter();
            },
        });
    </script>
@endsection



    


