@extends('layouts.task.master')
@section('content')
@section('title','|Inicio')
    <div class="container-fluid">
        <div class="row justify-content-center mt-5">
            <div class="col-10 col-sm-8 col-md-6 col-lg-4 col-xl-4 col-xxl-4">
                <div class="card">
                    <div class="card-header text-center">Bienvenido a Meteor Task</div>
                    <div class="card-body">
                        Hola {{ Auth::user()->name}} {{ Auth::user()->last_name}} !!
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
