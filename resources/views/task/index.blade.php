@extends('layouts.task.master')
@section('title','|Tareas')
@section('content')
<style>
    [v-cloak] {
    display: none;   
}
</style>
@section('content')
    <div class="container-fluid" id="appTask">
        <div class="row justify-content-center mt-5 border border-2 rounded background_task" style="overflow:auto;">
            <form id="form_task">
                @csrf
                <div class="row mt-5">
                    <div class="col-md-10">
                        <input
                            class="form-control border border-secondary-subtle rounded"     
                            type="text"
                            name="task"
                            id="task"
                            v-model="task"
                            placeholder="Nueva tarea"
                        >
                    </div>
                    <div class="col-md-2">
                        <div class="d-grid gap-2">
                            <button 
                                class="btn btn-success"
                                type="button"
                                @click.prevent="register_task">
                                <i class="fa-regular fa-paper-plane"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-floating mt-2">
                    <textarea 
                        class="border border-secondary-subtle rounded description_area"
                        id="task_description"
                        name="task_description"
                        v-model="taskDescription"
                        placeholder="Descripción de la tarea">
                    </textarea>
                </div>
            </form>
            <table v-if="(taskList.length > 0)" class="table table-borderless">
                <thead>
                    <th></th>
                    <th></th>
                    <th></th>
                </thead>
                <tbody>
                    <tr v-cloak v-for="(task, index) in taskList" 
                        :class="{'text-decoration-line-through': task.finished == 1}">
                        <td> 
                            <input 
                                class="form-check-input bg-success"
                                id="checkbox"
                                type="checkbox"
                                :checked="task.finished == 1"
                                @click.prevent="finished(task,index)"
                            >
                        </td>
                        <td> 
                            <h2 class="fw-lighter">
                                @{{ task.task }}
                            </h2>
                            <p>@{{ task.description }}</p>
                        </td>
                        <td> 
                            <a 
                                class="text-success"
                                href="#"
                                type="button"
                                @click.prevent="finished(task,index)">
                                <i class="fa-solid fa-globe"></i>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('script')
    @include('include.vue')
    <script>
        new Vue({
            el:'#appTask',
            data:{
                taskList:[],
                task:'',
                taskDescription:''
            },
            methods: {
                register_task(){
                    let token = $('input[name=_token]').val();
                    let task = document.getElementById('task')
                    let task_description = document.getElementById('task_description')
                    
                    if(this.validate_field(this.task)){
                        tesk.focus()
                        Swal.fire('¡Falta tarea!','Complete campo tarea','warning')
                        return
                    }

                    if(this.validate_field(this.task_description)){
                        task_description.focus()
                        Swal.fire('¡Falta descripción tarea!','Complete campo descripción tarea','warning')
                        return
                    }

                    axios.post('tasks',{
                        _token           : token,
                        task             :this.task,
                        task_description :this.taskDescription
                    }).then(response=>{
                        if(response.data != 0 ){
                            Swal.fire('¡Se ha creado tarea!', 'Correctamente!', 'success');
                            this.cleanForm();
                            this.getTasks();
                        }else{
                            Swal.fire('¡No se pudo crear Tarea!', 'Intente de nuevo', 'warning');
                            this.cleanForm();
                            return;
                        }
                    }).catch(error=>{
                        console.log(error)
                    })      
                },
                validate_field(field){ 
                    return field == '' ? true : false 
                },
                cleanForm(){
                    this.task = '';
                    this.taskDescription = ''
                },
                finished(task, index){
                    if(task.finished == 1){
                        Swal.fire('¡La Tarea se encuenta finalizada!', '', 'warning');
                        return;
                    }
                    
                    Swal.fire({
                        icon: 'warning',
                        title: `¿Estas seguro de finalizar la tarea:"${task.task}" ?`, 
                        showDenyButton: false,
                        showCancelButton: true,
                        confirmButtonText: `Finalizar`,
                        cancelButtonText: `Cancelar`,
                    }).then((result) => {
                        if (result.isConfirmed) {
                            let token = document.querySelector('input[name=_token]').value;
                            let idTask = task.idTask
                            axios.post('task/finished',{
                                _token: token,
                                idTask:idTask,
                            }).then(response => {
                                if(response.data != 0 ){
                                    Swal.fire('¡Se ha finalizado tarea!', 'Correctamente!', 'success');
                                    this.getTasks();
                                }else{
                                    Swal.fire('¡No se pudo finalizar Tarea!', 'Intente de nuevo', 'warning');
                                    return;
                                }
                            }).catch(error=>{
                                console.log(error)
                            }) 
                        }
                    });      
                },
                getTasks(){
                    axios.get('/task/getTask')
                    .then(response=>{
                        this.taskList = response.data
                        console.log(this.taskList)
                    })
                    .catch(error=> {
                        console.log(error)
                    })
                },
            },
            mounted() {
                this.getTasks();
            },
        })
    </script>
@endsection